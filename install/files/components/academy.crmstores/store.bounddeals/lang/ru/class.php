<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['CRMSTORES_NO_CRM_MODULE'] = 'Модуль CRM не установлен.';
$MESS['CRMSTORES_NO_MODULE'] = 'Модуль "Торговые точки CRM" не установлен.';
$MESS['CRMSTORES_STORE_NOT_FOUND'] = 'Торговая точка не существует.';
