<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['CRMSTORES_NO_CRM_MODULE'] = 'Модуль CRM не установлен.';
$MESS['CRMSTORES_ACTION_VIEW_TITLE'] = 'Просмотреть торговую точку';
$MESS['CRMSTORES_ACTION_VIEW_TEXT'] = 'Просмотреть';
$MESS['CRMSTORES_ACTION_EDIT_TITLE'] = 'Редактировать торговую точку';
$MESS['CRMSTORES_ACTION_EDIT_TEXT'] = 'Редактировать';
$MESS['CRMSTORES_ACTION_DELETE_TITLE'] = 'Удалить торговую точку';
$MESS['CRMSTORES_ACTION_DELETE_TEXT'] = 'Удалить';
$MESS['CRMSTORES_GRID_ACTION_DELETE_TITLE'] = 'Удалить отмеченные элементы';
$MESS['CRMSTORES_GRID_ACTION_DELETE_TEXT'] = 'Удалить';
$MESS['CRMSTORES_DELETE_DIALOG_TITLE'] = 'Удалить торговую точку';
$MESS['CRMSTORES_DELETE_DIALOG_MESSAGE'] = 'Вы уверены, что хотите удалить выбранную торговую точку?';
$MESS['CRMSTORES_DELETE_DIALOG_BUTTON'] = 'Удалить';
$MESS['CRM_ALL'] = 'Всего';
$MESS['CRM_SHOW_ROW_COUNT'] = 'Показать количество';
