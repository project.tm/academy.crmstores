<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['ACADEMY_CRMSTORES.MODULE_NAME'] = 'Торговые точки CRM';
$MESS['ACADEMY_CRMSTORES.MODULE_DESC'] = 'Управление торговыми точками из CRM';
$MESS['ACADEMY_CRMSTORES.PARTNER_NAME'] = 'Академия 1С-Битрикс';
$MESS['ACADEMY_CRMSTORES.PARTNER_URI'] = 'https://academy.1c-bitrix.ru/';
