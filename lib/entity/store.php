<?php

namespace Academy\CrmStores\Entity;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\IntegerField;
use Bitrix\Main\Entity\ReferenceField;
use Bitrix\Main\Entity\StringField;
use Bitrix\Main\UserTable;

class StoreTable extends DataManager
{
    public static function getTableName()
    {
        return 'academy_crmstores_store';
    }

    public static function getMap()
    {
        return array(
            new IntegerField('ID', array('primary' => true, 'autocomplete' => true)),
            new StringField('NAME'),
            new StringField('ADDRESS'),
            new IntegerField('ASSIGNED_BY_ID'),

            new ReferenceField(
                'ASSIGNED_BY',
                UserTable::getEntity(),
                array('=this.ASSIGNED_BY_ID' => 'ref.ID')
            )
        );
    }
}
